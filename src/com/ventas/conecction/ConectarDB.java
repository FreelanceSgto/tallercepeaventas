package com.ventas.conecction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Hashtable;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ConectarDB {

    private String dataSource = "jdbc/ventas";
    
    public Connection getConnection(Properties p){
        Connection cn = null;
        try {
            Class.forName(p.getProperty("driver"));
            cn = DriverManager.getConnection(p.getProperty("url") + p.getProperty("db"), p);
        } catch (Exception e) {
            e.printStackTrace();
            cn = null;
        }
                
        return  cn;
    }
    
    public Connection getConnection(){
        Connection cn = null;
        
        try {
            Hashtable htb = new Hashtable();
            htb.put(Context.INITIAL_CONTEXT_FACTORY, "com/datasource/context");
            Context context = new InitialContext(htb);
            //Context context = new InitialContext();
            Context context2 = (Context) context.lookup("java:comp/env");
            
            DataSource dataSource2 = (DataSource) context2.lookup(dataSource);
            
            cn = dataSource2.getConnection();
            
        } catch (Exception e) {            
            System.out.println(e);
            e.printStackTrace();
            cn = null;
        }
        
        
        return  cn;
    }
    
    
    
}
