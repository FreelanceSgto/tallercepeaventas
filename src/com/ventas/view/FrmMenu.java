package com.ventas.view;

import com.ventas.view.controller.UsuarioViewController;
import com.ventas.view.controller.UtilViewController;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;


import javax.swing.JTextField;

public class FrmMenu extends javax.swing.JFrame {

    private UsuarioViewController controllerVistaUser;
    private UtilViewController utilViewController;
    private boolean isActive;
    private List<JPanel> lstPanel;
            
            
    public FrmMenu() {
        initComponents();
    }

    public FrmMenu(JTextField user) {
        initComponents();
        this.setLocationRelativeTo(null);
        
        controllerVistaUser = new UsuarioViewController();
        utilViewController = new UtilViewController();
        
        this.isActive = true;
        this.tbDataUser.setModel(controllerVistaUser.dfVistaUser);
        
        lstPanel = new ArrayList<JPanel>();
        lstPanel.add(pnContenidoVentas);        
        lstPanel.add(pnContenidoProductos);
        lstPanel.add(pnContenidoUsuario);
        utilViewController.showHidePanel(lstPanel, 0);
                                        
        lbMenuUsuario.setText("Bienvenido: " + user.getText());
        
        pnContenidoVentas.add(new UtilViewController("fondo_productos.jpg"));
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnGeneral = new javax.swing.JPanel();
        pnMenu = new javax.swing.JPanel();
        lbOcultarMostrar = new javax.swing.JLabel();
        lbVentas = new javax.swing.JLabel();
        lbProductos = new javax.swing.JLabel();
        lbUsuarios = new javax.swing.JLabel();
        lbEspacioMenu = new javax.swing.JLabel();
        pnTitulo = new javax.swing.JPanel();
        lbMenuTitulo = new javax.swing.JLabel();
        lbMenuSalir = new javax.swing.JLabel();
        lbMenuUsuario = new javax.swing.JLabel();
        pnContenidoProductos = new javax.swing.JPanel();
        lbIdProducto = new javax.swing.JLabel();
        txIdProducto = new javax.swing.JTextField();
        lbDescripcion = new javax.swing.JLabel();
        txDescripcion = new javax.swing.JTextField();
        lbStock = new javax.swing.JLabel();
        txStock = new javax.swing.JTextField();
        lbPrecioBase = new javax.swing.JLabel();
        txPrecioBase = new javax.swing.JTextField();
        btGuardarProducto = new javax.swing.JButton();
        btEliminarProducto = new javax.swing.JButton();
        btModificarProducto = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbProductos = new javax.swing.JTable();
        lbTitulo = new javax.swing.JLabel();
        lbEspacioTituloTextField = new javax.swing.JLabel();
        lbEspacioTextFieldLabel = new javax.swing.JLabel();
        lbEspacioTextFieldTable = new javax.swing.JLabel();
        lbEspacioTextFieldButton = new javax.swing.JLabel();
        lbImagen = new javax.swing.JLabel();
        btBuscarProducto = new javax.swing.JButton();
        lbEspacioImagen = new javax.swing.JLabel();
        lbFondoProducto = new javax.swing.JLabel();
        pnContenidoVentas = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        pnContenidoUsuario = new javax.swing.JPanel();
        lbTituloUsuario = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbDataUser = new javax.swing.JTable();
        lbImagenUser = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        lbEspacioTituloTextFieldUsuario = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lbContrasenia = new javax.swing.JLabel();
        txUsuario = new javax.swing.JTextField();
        txContrasenia = new javax.swing.JTextField();
        lbCorreo = new javax.swing.JLabel();
        btGuardar = new javax.swing.JButton();
        btEliminar = new javax.swing.JButton();
        btModificar = new javax.swing.JButton();
        lbUsuario = new javax.swing.JLabel();
        txCorreo = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnGeneral.setPreferredSize(new java.awt.Dimension(700, 500));
        pnGeneral.setLayout(new java.awt.GridBagLayout());

        pnMenu.setBackground(new java.awt.Color(0, 204, 255));
        pnMenu.setPreferredSize(new java.awt.Dimension(150, 400));
        pnMenu.setLayout(new java.awt.GridBagLayout());

        lbOcultarMostrar.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lbOcultarMostrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ventas/IMG/ocultar_menu.png"))); // NOI18N
        lbOcultarMostrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbOcultarMostrarMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.weightx = 0.1;
        pnMenu.add(lbOcultarMostrar, gridBagConstraints);

        lbVentas.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lbVentas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ventas/IMG/username.png"))); // NOI18N
        lbVentas.setText("Ventas");
        lbVentas.setPreferredSize(new java.awt.Dimension(70, 50));
        lbVentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbVentasMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 10;
        pnMenu.add(lbVentas, gridBagConstraints);

        lbProductos.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lbProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ventas/IMG/username.png"))); // NOI18N
        lbProductos.setText("Productos");
        lbProductos.setPreferredSize(new java.awt.Dimension(95, 50));
        lbProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbProductosMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 10;
        pnMenu.add(lbProductos, gridBagConstraints);

        lbUsuarios.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lbUsuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ventas/IMG/username.png"))); // NOI18N
        lbUsuarios.setText("Usuarios");
        lbUsuarios.setPreferredSize(new java.awt.Dimension(83, 50));
        lbUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbUsuariosMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 10;
        pnMenu.add(lbUsuarios, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.1;
        pnMenu.add(lbEspacioMenu, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 80;
        gridBagConstraints.ipady = 400;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnGeneral.add(pnMenu, gridBagConstraints);

        pnTitulo.setBackground(new java.awt.Color(255, 204, 0));
        pnTitulo.setPreferredSize(new java.awt.Dimension(700, 110));
        pnTitulo.setLayout(new java.awt.GridBagLayout());

        lbMenuTitulo.setFont(new java.awt.Font("Georgia", 1, 24)); // NOI18N
        lbMenuTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbMenuTitulo.setText("HIPERMERCADO PEPITO");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        pnTitulo.add(lbMenuTitulo, gridBagConstraints);

        lbMenuSalir.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lbMenuSalir.setText("Salir");
        lbMenuSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbMenuSalirMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnTitulo.add(lbMenuSalir, gridBagConstraints);

        lbMenuUsuario.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lbMenuUsuario.setText("Bienvenido: User");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnTitulo.add(lbMenuUsuario, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 700;
        gridBagConstraints.ipady = 110;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnGeneral.add(pnTitulo, gridBagConstraints);

        pnContenidoProductos.setBackground(new java.awt.Color(113, 240, 97));
        pnContenidoProductos.setPreferredSize(new java.awt.Dimension(600, 400));
        pnContenidoProductos.setLayout(new java.awt.GridBagLayout());

        lbIdProducto.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lbIdProducto.setText("IdProducto:");
        lbIdProducto.setPreferredSize(new java.awt.Dimension(88, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 15;
        pnContenidoProductos.add(lbIdProducto, gridBagConstraints);

        txIdProducto.setPreferredSize(new java.awt.Dimension(6, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 116;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnContenidoProductos.add(txIdProducto, gridBagConstraints);

        lbDescripcion.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lbDescripcion.setText("Descripcion:");
        lbDescripcion.setPreferredSize(new java.awt.Dimension(92, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 15;
        pnContenidoProductos.add(lbDescripcion, gridBagConstraints);

        txDescripcion.setPreferredSize(new java.awt.Dimension(6, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 116;
        pnContenidoProductos.add(txDescripcion, gridBagConstraints);

        lbStock.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lbStock.setText("Stock:");
        lbStock.setPreferredSize(new java.awt.Dimension(90, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 15;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnContenidoProductos.add(lbStock, gridBagConstraints);

        txStock.setPreferredSize(new java.awt.Dimension(6, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 116;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnContenidoProductos.add(txStock, gridBagConstraints);

        lbPrecioBase.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lbPrecioBase.setText("Precio Base: ");
        lbPrecioBase.setPreferredSize(new java.awt.Dimension(90, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 15;
        pnContenidoProductos.add(lbPrecioBase, gridBagConstraints);

        txPrecioBase.setPreferredSize(new java.awt.Dimension(6, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 116;
        pnContenidoProductos.add(txPrecioBase, gridBagConstraints);

        btGuardarProducto.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        btGuardarProducto.setText("Guardar");
        btGuardarProducto.setPreferredSize(new java.awt.Dimension(100, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnContenidoProductos.add(btGuardarProducto, gridBagConstraints);

        btEliminarProducto.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        btEliminarProducto.setText("Eliminar");
        btEliminarProducto.setPreferredSize(new java.awt.Dimension(100, 30));
        btEliminarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarProductoActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnContenidoProductos.add(btEliminarProducto, gridBagConstraints);

        btModificarProducto.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        btModificarProducto.setText("Modificar");
        btModificarProducto.setPreferredSize(new java.awt.Dimension(100, 30));
        btModificarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btModificarProductoActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        pnContenidoProductos.add(btModificarProducto, gridBagConstraints);

        tbProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "IdProducto", "Descripcion", "Stock", "Precio Base"
            }
        ));
        tbProductos.setPreferredSize(new java.awt.Dimension(300, 30));
        jScrollPane2.setViewportView(tbProductos);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = -250;
        pnContenidoProductos.add(jScrollPane2, gridBagConstraints);

        lbTitulo.setFont(new java.awt.Font("Georgia", 1, 22)); // NOI18N
        lbTitulo.setText("GESTIÓN DE PRODUCTOS");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 8;
        pnContenidoProductos.add(lbTitulo, gridBagConstraints);

        lbEspacioTituloTextField.setAlignmentY(0.0F);
        lbEspacioTituloTextField.setPreferredSize(new java.awt.Dimension(0, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        pnContenidoProductos.add(lbEspacioTituloTextField, gridBagConstraints);

        lbEspacioTextFieldLabel.setPreferredSize(new java.awt.Dimension(34, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        pnContenidoProductos.add(lbEspacioTextFieldLabel, gridBagConstraints);

        lbEspacioTextFieldTable.setPreferredSize(new java.awt.Dimension(34, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 7;
        pnContenidoProductos.add(lbEspacioTextFieldTable, gridBagConstraints);

        lbEspacioTextFieldButton.setPreferredSize(new java.awt.Dimension(30, 14));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 4;
        pnContenidoProductos.add(lbEspacioTextFieldButton, gridBagConstraints);

        lbImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ventas/IMG/milo.png"))); // NOI18N
        lbImagen.setPreferredSize(new java.awt.Dimension(200, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridheight = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnContenidoProductos.add(lbImagen, gridBagConstraints);

        btBuscarProducto.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        btBuscarProducto.setText("Buscar producto");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        pnContenidoProductos.add(btBuscarProducto, gridBagConstraints);

        lbEspacioImagen.setPreferredSize(new java.awt.Dimension(30, 14));
        lbEspacioImagen.setRequestFocusEnabled(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        pnContenidoProductos.add(lbEspacioImagen, gridBagConstraints);

        lbFondoProducto.setText("sss");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        pnContenidoProductos.add(lbFondoProducto, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 580;
        gridBagConstraints.ipady = 400;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        pnGeneral.add(pnContenidoProductos, gridBagConstraints);

        pnContenidoVentas.setBackground(new java.awt.Color(204, 0, 51));
        pnContenidoVentas.setPreferredSize(new java.awt.Dimension(600, 400));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable1);

        jButton2.setText("jButton2");

        jLabel1.setText("jLabel1");

        jTextField1.setText("jTextField1");

        jTextField2.setText("jTextField1");

        jLabel2.setText("jLabel1");

        jTextField3.setText("jTextField1");

        jLabel3.setText("jLabel1");

        javax.swing.GroupLayout pnContenidoVentasLayout = new javax.swing.GroupLayout(pnContenidoVentas);
        pnContenidoVentas.setLayout(pnContenidoVentasLayout);
        pnContenidoVentasLayout.setHorizontalGroup(
            pnContenidoVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addGroup(pnContenidoVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        pnContenidoVentasLayout.setVerticalGroup(
            pnContenidoVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(pnContenidoVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1))
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnContenidoVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2))
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnContenidoVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnContenidoVentasLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel3))
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jButton2)
                .addGap(34, 34, 34)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 580;
        gridBagConstraints.ipady = 400;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        pnGeneral.add(pnContenidoVentas, gridBagConstraints);

        pnContenidoUsuario.setBackground(new java.awt.Color(255, 204, 204));
        pnContenidoUsuario.setPreferredSize(new java.awt.Dimension(600, 400));

        lbTituloUsuario.setFont(new java.awt.Font("Georgia", 1, 22)); // NOI18N
        lbTituloUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTituloUsuario.setText("GESTIÓN DE USUARIOS");

        jScrollPane1.setPreferredSize(new java.awt.Dimension(250, 200));

        tbDataUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbDataUser.setPreferredSize(new java.awt.Dimension(250, 200));
        jScrollPane1.setViewportView(tbDataUser);

        lbImagenUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbImagenUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/ventas/IMG/producto_ejemplo.jpg"))); // NOI18N
        lbImagenUser.setPreferredSize(new java.awt.Dimension(200, 30));

        jButton1.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        jButton1.setText("Buscar usuario");

        lbEspacioTituloTextFieldUsuario.setPreferredSize(new java.awt.Dimension(0, 30));

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        lbContrasenia.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        lbContrasenia.setText("Contraseña: ");
        lbContrasenia.setPreferredSize(new java.awt.Dimension(70, 30));

        txUsuario.setPreferredSize(new java.awt.Dimension(140, 30));

        txContrasenia.setPreferredSize(new java.awt.Dimension(140, 30));

        lbCorreo.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        lbCorreo.setText("Correo: ");
        lbCorreo.setPreferredSize(new java.awt.Dimension(70, 30));

        btGuardar.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        btGuardar.setText("Guardar");
        btGuardar.setPreferredSize(new java.awt.Dimension(100, 30));
        btGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGuardarActionPerformed(evt);
            }
        });

        btEliminar.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        btEliminar.setText("Eliminar");
        btEliminar.setPreferredSize(new java.awt.Dimension(100, 30));

        btModificar.setFont(new java.awt.Font("Georgia", 1, 12)); // NOI18N
        btModificar.setText("Modificar");
        btModificar.setPreferredSize(new java.awt.Dimension(100, 30));

        lbUsuario.setFont(new java.awt.Font("Georgia", 1, 14)); // NOI18N
        lbUsuario.setText("Usuario: ");
        lbUsuario.setPreferredSize(new java.awt.Dimension(70, 30));

        txCorreo.setPreferredSize(new java.awt.Dimension(140, 30));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(lbCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(btGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(514, 514, 514)
                        .addComponent(btEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(289, 289, 289)
                        .addComponent(btModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(btEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnContenidoUsuarioLayout = new javax.swing.GroupLayout(pnContenidoUsuario);
        pnContenidoUsuario.setLayout(pnContenidoUsuarioLayout);
        pnContenidoUsuarioLayout.setHorizontalGroup(
            pnContenidoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnContenidoUsuarioLayout.createSequentialGroup()
                .addGroup(pnContenidoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbEspacioTituloTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnContenidoUsuarioLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(pnContenidoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbImagenUser, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(pnContenidoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbTituloUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 638, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnContenidoUsuarioLayout.setVerticalGroup(
            pnContenidoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnContenidoUsuarioLayout.createSequentialGroup()
                .addComponent(lbEspacioTituloTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbTituloUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnContenidoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnContenidoUsuarioLayout.createSequentialGroup()
                        .addComponent(lbImagenUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnContenidoUsuarioLayout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 580;
        gridBagConstraints.ipady = 400;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        pnGeneral.add(pnContenidoUsuario, gridBagConstraints);

        getContentPane().add(pnGeneral, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lbUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbUsuariosMouseClicked
        pnContenidoUsuario.add(new UtilViewController("fondo_login.png"));
        pnContenidoUsuario.repaint();
        
        utilViewController.showHidePanel(lstPanel, 2);
        controllerVistaUser.removeItemsUser(tbDataUser);
        controllerVistaUser.loadDataUser(controllerVistaUser.dfVistaUser, tbDataUser);
        
        
        
        /**
         * Imagen Imagen = new Imagen(); pnContenidoUsuario.add(Imagen);
         * pnContenidoUsuario.repaint();
        **
         */
    }//GEN-LAST:event_lbUsuariosMouseClicked

    private void lbProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbProductosMouseClicked
        utilViewController.showHidePanel(lstPanel, 1);
    }//GEN-LAST:event_lbProductosMouseClicked

    private void btGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGuardarActionPerformed
        new UsuarioViewController().saveUser(txUsuario, txContrasenia, txCorreo, tbDataUser, controllerVistaUser.dfVistaUser);
    }//GEN-LAST:event_btGuardarActionPerformed

    private void lbMenuSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbMenuSalirMouseClicked
        new UtilViewController().showFormulario(new FrmInicioSesion(), this);
    }//GEN-LAST:event_lbMenuSalirMouseClicked

    private void lbVentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbVentasMouseClicked
        utilViewController.showHidePanel(lstPanel, 0);
        /***pnContenidoVentas.add(new UtilViewController("fondo_productos.jpg"));
        pnContenidoVentas.repaint();**/
    }//GEN-LAST:event_lbVentasMouseClicked

    private void btEliminarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarProductoActionPerformed
    }//GEN-LAST:event_btEliminarProductoActionPerformed

    private void btModificarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btModificarProductoActionPerformed
    }//GEN-LAST:event_btModificarProductoActionPerformed

    private void lbOcultarMostrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbOcultarMostrarMouseClicked
        this.isActive =  new UtilViewController().isShowOrHideMenu(pnGeneral, pnMenu, lbOcultarMostrar, lbVentas, lbProductos, lbUsuarios, isActive);
    }//GEN-LAST:event_lbOcultarMostrarMouseClicked
    
    
    
    
    
    
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new FrmMenu().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btBuscarProducto;
    private javax.swing.JButton btEliminar;
    private javax.swing.JButton btEliminarProducto;
    private javax.swing.JButton btGuardar;
    private javax.swing.JButton btGuardarProducto;
    private javax.swing.JButton btModificar;
    private javax.swing.JButton btModificarProducto;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JLabel lbContrasenia;
    private javax.swing.JLabel lbCorreo;
    private javax.swing.JLabel lbDescripcion;
    private javax.swing.JLabel lbEspacioImagen;
    private javax.swing.JLabel lbEspacioMenu;
    private javax.swing.JLabel lbEspacioTextFieldButton;
    private javax.swing.JLabel lbEspacioTextFieldLabel;
    private javax.swing.JLabel lbEspacioTextFieldTable;
    private javax.swing.JLabel lbEspacioTituloTextField;
    private javax.swing.JLabel lbEspacioTituloTextFieldUsuario;
    private javax.swing.JLabel lbFondoProducto;
    private javax.swing.JLabel lbIdProducto;
    private javax.swing.JLabel lbImagen;
    private javax.swing.JLabel lbImagenUser;
    private javax.swing.JLabel lbMenuSalir;
    private javax.swing.JLabel lbMenuTitulo;
    private javax.swing.JLabel lbMenuUsuario;
    private javax.swing.JLabel lbOcultarMostrar;
    private javax.swing.JLabel lbPrecioBase;
    private javax.swing.JLabel lbProductos;
    private javax.swing.JLabel lbStock;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JLabel lbTituloUsuario;
    private javax.swing.JLabel lbUsuario;
    private javax.swing.JLabel lbUsuarios;
    private javax.swing.JLabel lbVentas;
    private javax.swing.JPanel pnContenidoProductos;
    private javax.swing.JPanel pnContenidoUsuario;
    private javax.swing.JPanel pnContenidoVentas;
    private javax.swing.JPanel pnGeneral;
    private javax.swing.JPanel pnMenu;
    private javax.swing.JPanel pnTitulo;
    private javax.swing.JTable tbDataUser;
    private javax.swing.JTable tbProductos;
    private javax.swing.JTextField txContrasenia;
    private javax.swing.JTextField txCorreo;
    private javax.swing.JTextField txDescripcion;
    private javax.swing.JTextField txIdProducto;
    private javax.swing.JTextField txPrecioBase;
    private javax.swing.JTextField txStock;
    private javax.swing.JTextField txUsuario;
    // End of variables declaration//GEN-END:variables
}

/***
    public class Imagen extends javax.swing.JPanel {

        public Imagen() {
            this.setSize(2000, 800); //se selecciona el tamaño del panel
        }

//Se crea un método cuyo parámetro debe ser un objeto Graphics
        public void paint(Graphics grafico) {
            Dimension height = getSize();

//Se selecciona la imagen que tenemos en el paquete de la //ruta del programa
            ImageIcon Img = new ImageIcon(getClass().getResource("/com/ventas/IMG/fondo_productos.jpg"));

//se dibuja la imagen que tenemos en el paquete Images //dentro de un panel
            grafico.drawImage(Img.getImage(), 0, 0, height.width, height.height, null);

            setOpaque(false);
            super.paintComponent(grafico);
        }
    }
***/
    /**
     * public JPanel pnEjemplo = new JPanel(){
     *
     * public void paintComponent(Graphics g){ Image image = new
     * ImageIcon(this.getClass().getResource("/com/ventas/IMG/fondo_productos.jpg")).getImage();
     * g.drawImage(image , 0, 0, getWidth(), getHeight(),this); } };*
     */