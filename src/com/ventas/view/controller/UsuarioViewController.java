package com.ventas.view.controller;

import com.ventas.controller.UsuarioController;
import com.ventas.model.Usuario;
import com.ventas.DTO.UsuarioDTO;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class UsuarioViewController {
    
    String dataUser [][] = {};
    String cabezeraUser[] = {"Usuario", "Contrasenia", "Correo2"};
    public DefaultTableModel dfVistaUser = new DefaultTableModel(dataUser, cabezeraUser);
    
    public void saveUser(JTextField usuario, JTextField contrasenia, JTextField correo, JTable dataUser, DefaultTableModel df){        
        df.addRow(new UsuarioController().addUserInTable(usuario.getText(), contrasenia.getText(), correo.getText()));
        dataUser.setModel(df);
        
    }
    
    public void removeItemsUser(JTable dataUser){
        int size = dataUser.getRowCount();
        DefaultTableModel df = (DefaultTableModel)dataUser.getModel();
        for(int x = 0; x< size; x++)
            df.removeRow(0);
    }
    
    public void loadDataUser(DefaultTableModel dft, JTable tableUser){
            for(ArrayList user: UsuarioController.getListUser()){
                Object us[] = {user.get(0), user.get(1), user.get(2)};
                dft.addRow(us);
                tableUser.setModel(dft);
            }
    }
    
    
    
}
