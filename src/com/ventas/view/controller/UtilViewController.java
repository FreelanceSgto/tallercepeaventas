
package com.ventas.view.controller;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class UtilViewController extends javax.swing.JPanel{
    
    private String nameImage;
    
    public UtilViewController(){
        
    }
    
    public UtilViewController(String nameImage){
        this.setSize(2000, 800);
        this.nameImage = nameImage;
    }
    
        //Se crea un método cuyo parámetro debe ser un objeto Graphics
        public void paint(Graphics grafico) {
            Dimension height = getSize();

            //Se selecciona la imagen que tenemos en el paquete de la //ruta del programa
            ImageIcon Img = new ImageIcon(getClass().getResource("/com/ventas/IMG/" + this.nameImage));

            //se dibuja la imagen que tenemos en el paquete Images //dentro de un panel
            grafico.drawImage(Img.getImage(), 0, 0, height.width, height.height, null);

            setOpaque(false);
            super.paintComponent(grafico);
        }
    
    
    public void showIcon(JLabel label, String nameFile){
        label.setIcon(new ImageIcon(getClass().getResource("/com/ventas/IMG/" + nameFile)));        
    }
    
    
    public void showFormulario(JFrame frmVisible, JFrame frmOculto){
        frmVisible.setVisible(true);
        frmOculto.setVisible(false);
    }
    
    public void hideMenu(JPanel panel, JLabel label, int posicion, boolean isFinalComponent){
        label.setText("");
        label.setPreferredSize(new Dimension(0, posicion == 0 ? 32 : 50));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = posicion;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 10;
        if(posicion == 0) gridBagConstraints.weightx = 0.1;
        panel.add(label, gridBagConstraints);
        if(isFinalComponent)panel.repaint();
    }
    
    public void hideShowMenuPanel(JPanel pnGeneral, JPanel pnMenu, boolean isActive){
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = isActive ? 0 : 80;
        gridBagConstraints.ipady = 400;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        pnGeneral.add(pnMenu, gridBagConstraints);
        pnGeneral.repaint();
    }
    
    public void setNameLabel(JLabel label1, String name1, JLabel label2, String name2, JLabel label3, String name3, JLabel label4, String name4){
        if(label1!= null) label1.setText(name1);
        if(label2!= null) label2.setText(name2);        
        if(label3!= null) label3.setText(name3);
        if(label4!= null) label4.setText(name4);
    }
    
    public boolean isShowOrHideMenu(JPanel pnGeneral, JPanel pnMenu, JLabel lbOcultarMostrar, JLabel lbVentas, JLabel lbProductos, JLabel lbUsuarios, boolean isActive){
        UtilViewController utilViewController = new UtilViewController();
        String img ="";
        if (isActive) {
            img = "mostrar_menu.png";            
            utilViewController.hideMenu(pnMenu, lbOcultarMostrar, 0, false);
            utilViewController.hideMenu(pnMenu, lbVentas, 1, false);
            utilViewController.hideMenu(pnMenu, lbProductos, 2, false);
            utilViewController.hideMenu(pnMenu, lbUsuarios, 3, true);            
            isActive = false;
        } else {
            img = "ocultar_menu.png";
            utilViewController.setNameLabel(lbOcultarMostrar, "", lbVentas, "Ventas", lbProductos, "Productos", lbUsuarios, "Usuarios");
            isActive = true;            
        }
        utilViewController.hideShowMenuPanel(pnGeneral, pnMenu, !isActive);
        utilViewController.showIcon(lbOcultarMostrar, img);
        return isActive;
    }
    
    public void showHidePanel(List<JPanel> lstPanel, int posicionHabilitada){        
        for(int x = 0; x < lstPanel.size(); x++ ){
            lstPanel.get(x).setVisible(x == posicionHabilitada ? true : false);
        }        
    }
            
    
    /**
    public void showMenu(){
        
            

            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 1;
            gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
            gridBagConstraints.ipadx = 80;
            gridBagConstraints.ipady = 400;
            gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
            pnGeneral.add(pnMenu, gridBagConstraints);
            pnGeneral.repaint();
        
        
        
    }
    **/
}


/**
        lbOcultarMostrar.setText("");        
        lbOcultarMostrar.setPreferredSize(new Dimension(0, 32));        
        GridBagConstraints gridBagConstraints0 = new GridBagConstraints();
        gridBagConstraints0.gridx = 0;
        gridBagConstraints0.gridy = 0;
        gridBagConstraints0.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints0.ipady = 10;
        gridBagConstraints0.weightx = 0.1;
        pnMenu.add(lbOcultarMostrar, gridBagConstraints0);
        
        
        lbVentas.setText("");
        lbVentas.setPreferredSize(new Dimension(0, 50));        
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints1.ipady = 10;
        pnMenu.add(lbVentas, gridBagConstraints1);
        
        
        lbProductos.setText("");
        lbProductos.setPreferredSize(new Dimension(0, 50));
        GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
        gridBagConstraints3.gridx = 0;
        gridBagConstraints3.gridy = 2;
        gridBagConstraints3.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints3.ipady = 10;        
        pnMenu.add(lbProductos, gridBagConstraints3);
        
                
        lbUsuarios.setText("");
        lbUsuarios.setPreferredSize(new Dimension(0, 50));
        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 3;
        gridBagConstraints2.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints2.ipady = 10;        
        pnMenu.add(lbUsuarios, gridBagConstraints2);
        **/
