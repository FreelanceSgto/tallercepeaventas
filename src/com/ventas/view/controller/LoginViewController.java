/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ventas.view.controller;

import com.ventas.controller.LoginController;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class LoginViewController {

    public void getAcces(JTextField usuario, JTextField contrasenia, JFrame frmMenu, JFrame frmLogin){
        if (new LoginController().userExists(usuario.getText(), contrasenia.getText()))
            new UtilViewController().showFormulario(frmMenu, frmLogin);
        else
            JOptionPane.showMessageDialog(null, "Usuario ingresado no existe");
    }
    
    

}
