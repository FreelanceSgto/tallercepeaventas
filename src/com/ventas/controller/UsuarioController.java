package com.ventas.controller;

import com.ventas.model.Usuario;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;


public class UsuarioController {
    public static List<Usuario> userList = new ArrayList<Usuario>();
    
               
    /**public UsuarioController(){
        userList = new ArrayList<Usuario>();
    }**/
    
    public static List<Usuario> getUserList(){
        return userList;
    }
    
    public static void setUserList(Usuario usuario){
        userList.add(usuario);
    }
    
    /*
    public static void addUsers(Usuario user){        
        getUserList().add(user);        
    }**/

    
    
    public Vector addUserInTable(String usuario, String contrasenia, String correo){
        
        Usuario usuarioModel = new Usuario();
        usuarioModel.setUsuario(usuario);
        usuarioModel.setContrasenia(contrasenia);
        usuarioModel.setCorreo(correo);
        setUserList(usuarioModel);
        
        Vector lst = new Vector();                
        lst.add(usuario);
        lst.add(contrasenia);
        lst.add(correo);
        
        return lst;
    }
       
    public static List<ArrayList> getListUser(){
        
        List<ArrayList> lstArray = new ArrayList<ArrayList>();
        ArrayList arrayUser;
        
        for(Usuario lst: getUserList()){
            arrayUser = new ArrayList();
            arrayUser.add(lst.getUsuario());
            arrayUser.add(lst.getContrasenia());
            arrayUser.add(lst.getCorreo());
            
            lstArray.add(arrayUser);
        }                
        return lstArray;
    }
    
}
