package com.ventas.controller;

import com.ventas.model.Usuario;
import java.util.List;

public class LoginController {

    
    public boolean userExists(String user, String password){
        if(UsuarioController.getUserList().isEmpty()) loadUserAdmin();
        for(Usuario lst : UsuarioController.getUserList()){
            if(lst.getUsuario().equals(user) && lst.getContrasenia().equals(password) )                
                return true;            
        }
        return false;
    }
    
    public void loadUserAdmin(){
        Usuario user = new Usuario();
        user.setUsuario("admin");
        user.setContrasenia("admin");
        UsuarioController.getUserList().add(user);
    }
    
}
