package com.ventas.controller;

import com.ventas.conecction.ConectarDB;
import com.ventas.view.FrmInicioSesion;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;


public class StartApp {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String path = args[0];
        String nameFile = "dbCredenciales.properties";
        
        Properties property = new Properties();
        property.load(new FileReader(path + nameFile));
        
        Connection con = new ConectarDB().getConnection(property);
        
        if(con != null){
            System.out.println("Coneccion exitosa");
            new FrmInicioSesion().setVisible(true);
        }
        else
            System.out.println("coneccion Erronea");                
                
    }
    
    /**public InputStream getInputStream(String nameProperties){
        return getClass().getClassLoader().getResourceAsStream(nameProperties);
    }**/
    
}


//property.load(new FileInputStream("D:\\ProyectosLocales\\tallercepeaventas\\src\\tallercepeaventas\\dbCredenciales.properties"));
        //ClassLoader loader = Thread.currentThread().getContextClassLoader();
        //property.load(loader.getResourceAsStream("../com/taller/config/files/dbCredenciales.properties"));        